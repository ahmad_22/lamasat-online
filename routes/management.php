<?php

use App\Http\Controllers\Customer\CategoryController;
use Illuminate\Support\Facades\Route;


Route::prefix('category')->group(function () {
    Route::post('/create', [CategoryController::class, 'create']);
    Route::put('/update/{categoryId}', [CategoryController::class, 'update']);
    Route::delete('/delete/{categoryId}', [CategoryController::class, 'delete']);
    Route::get('/all', [CategoryController::class, 'all']);
    Route::get('/find/{categoryId}', [CategoryController::class, 'find']);
});
