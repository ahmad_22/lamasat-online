<?php

use App\Http\Controllers\Customer\CategoryController;
use App\Http\Controllers\Customer\CustomerController;
use Illuminate\Support\Facades\Route;


Route::get('/all',[CustomerController::class,'all']);
Route::post('/create',[CustomerController::class,'create']);
Route::put('/update/{customerId}',[CustomerController::class,'update']);
Route::get('/find/{customerId}',[CustomerController::class,'find']);

Route::delete('/delete/{customerId}',[CustomerController::class,'delete']);

