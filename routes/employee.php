<?php

use App\Http\Controllers\Employee\BranchController;
use App\Http\Controllers\Employee\DepartmentController;
use App\Http\Controllers\Employee\EmployeeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


Route::post('/login',[UserController::class,'login']);
Route::post('/create', [EmployeeController::class, 'create']);
Route::put('/update/{employeeId}', [EmployeeController::class, 'update']);
Route::delete('/delete/{employeeId}', [EmployeeController::class, 'delete']);
Route::get('/all', [EmployeeController::class, 'all']);
Route::get('/find/{employeeId}', [EmployeeController::class, 'find']);


Route::prefix('department')->group(function () {

    Route::post('/create', [DepartmentController::class, 'create']);
    Route::put('/update/{departmentId}', [DepartmentController::class, 'update']);
    Route::delete('/delete/{departmentId}', [DepartmentController::class, 'delete']);
    Route::get('/all', [DepartmentController::class, 'all']);
    Route::get('/find/{departmentId}', [DepartmentController::class, 'find']);
});

Route::prefix('branch')->group(function () {

    Route::post('/create', [BranchController::class, 'create']);
    Route::put('/update/{branchId}', [BranchController::class, 'update']);
    Route::delete('/delete/{branchId}', [BranchController::class, 'delete']);
    Route::get('/all', [BranchController::class, 'all']);
    Route::get('/find/{employeeId}', [BranchController::class, 'find']);
});
