<?php



namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Branch extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'branches';

    protected $fillable = ['name', 'location', 'note'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [];

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
