<?php



namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class HomeworkDaySwitsh extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'homework_day_switshes';

    protected $fillable = ['is_aprove','send_id','receive_id','homework_day_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [];

}
