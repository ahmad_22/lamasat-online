<?php



namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Salary extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'salaries';

    protected $fillable = ['amount', 'date', 'note', 'typeDiscount', 'discount', 'amountBefor', 'employee_id'];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];
}
