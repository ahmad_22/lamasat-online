<?php



namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class HomeworkDay extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'homework_days';

    protected $fillable = ['name', 'note'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [];

}
