<?php



namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class HomeworkDayTodo extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'homework_day_todos';

    protected $fillable = ['employee_id', 'homework_day_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [];
}
