<?php



namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class SalaryExtra extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'salary_extras';

    protected $fillable = ['extra_type', 'note', 'type', 'amount', 'fullamount', 'percentage', 'employee_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [];
}
