<?php



namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Kouja\ProjectAssistant\Bases\BaseModel;

class TimeRecord extends BaseModel
{
    use HasFactory;

    protected $table = 'time_records';

    protected $fillable = ['start_work','end_work','note','user_id'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [];

}
