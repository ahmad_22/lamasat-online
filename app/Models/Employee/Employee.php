<?php



namespace App\Models\Employee;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Employee extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'employees';

    protected $fillable = [
        'full_name', 'gender', 'birthday', 'phone', 'phone2', 'address',
        'default_salary', 'type_salary', 'type_salary_of', 'branch_id', 'department_id'
    ];

    protected $hidden = ['update_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
