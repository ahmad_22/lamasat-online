<?php



namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Department extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'departments';

    protected $fillable = ['name', 'note'];

    protected $hidden = ['update_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];
}
