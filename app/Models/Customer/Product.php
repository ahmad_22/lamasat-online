<?php



namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Product extends BaseModel
{
    use HasFactory,SoftDeletes;

    protected $table = 'products';

    protected $fillable = [];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
