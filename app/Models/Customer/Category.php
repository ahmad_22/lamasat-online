<?php



namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Category extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'categories';

    protected $fillable = ['name' ,'description'];

    protected $hidden = [];

    protected $dates = [];

    protected $casts = [];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
