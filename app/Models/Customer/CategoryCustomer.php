<?php



namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class CategoryCustomer extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'category_customers';

    protected $fillable = ['discount', 'category_id', 'customer_id'];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];

}
