<?php



namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kouja\ProjectAssistant\Bases\BaseModel;

class Customer extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'customers';

    protected $fillable = ['name','serial','email','phone','phone2','type','address','is_charge','note'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $dates = [];

    protected $casts = [];

}
