<?php

namespace App\Models;

use App\Models\Employee\Employee;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
// use Laravel\Sanctum\HasApiTokens;
use Laravel\Passport\HasApiTokens;
use Kouja\ProjectAssistant\Traits\ModelTrait;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, ModelTrait;

    protected $fillable = ['username', 'email', 'password', 'user_scope', 'employee_id', 'email_verified_at'];

    protected $hidden = ['updated_at' ,'deleted_at','password'];

    protected $casts = ['email_verified_at' => 'datetime',];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function login($data)
    {
        $user = null;
        if (Auth::attempt($data)) {
            $user = Auth::user();
            $user->token = $user->createToken('lamsatprolect')->accessToken;
        }
        $user['employee'] = $user->employee;
        return $user;
    }
}
