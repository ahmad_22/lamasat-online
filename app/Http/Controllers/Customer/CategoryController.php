<?php

namespace App\Http\Controllers\Customer;

use App\Enums\CustomerTypeEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\Category\CreateCategoryRequest;
use App\Http\Requests\Customer\Category\DeleteCategoryRequest;
use App\Http\Requests\Customer\Category\UpdateCategoryRequest;
use App\Http\Requests\Employee\Employee\FindEmployeeRequest;
use App\Models\Customer\Category;
use App\Models\Customer\CategoryCustomer;
use App\Models\Customer\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class CategoryController extends Controller
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function create(CreateCategoryRequest $request)
    {
        if ($request->filled('customer_ids') && $request->filled('customer_type'))
            return ResponseHelper::invalidData();
        else if ($request->filled('customer_ids') && $request->get('is_all'))
            return ResponseHelper::invalidData();
        else if ($request->filled('customer_type') && $request->get('is_all'))
            return ResponseHelper::invalidData();

        $data = $request->only($this->category->getFillable());
        $created = $this->category->createData($data);

        $customerIds = $request->filled('customer_ids') ? $request->get('customer_ids') : 0;
        if ($customerIds == 0) {
            $customerIds = $request->get('is_all', false) ? (new Customer())->getData(function ($data) {
                $data->where('type', '<>', CustomerTypeEnum::direct);
            })->pluck('id')->toArray() : 0;
        }
        if ($customerIds == 0) {
            $customerIds = $request->filled('customer_type') ? (new Customer())
                ->getData(['type' => $request->get('customer_type')])->pluck('id')->toArray() : 0;
        }
        if ($customerIds != 0) {
            $categoryCustomerData = [];
            foreach ($customerIds as $id) {
                $temp['customer_id'] = $id;
                $temp['category_id'] = $created->id;
                $temp['discount'] = $request->get('discount');
                $temp['created_at'] = Carbon::now();
                $temp['updated_at'] = Carbon::now();
                array_push($categoryCustomerData, $temp);
            }
            (new CategoryCustomer())->insertData($categoryCustomerData);
        }

        return ResponseHelper::create($created);
    }

    public function update(UpdateCategoryRequest $request,$categoryId)
    {
        $data = $request->only($this->category->getFillable());
        $this->category->updateData(['id' => $categoryId], $data);
        return ResponseHelper::update();
    }

    public function delete(DeleteCategoryRequest $request,$categoryId)
    {
        //$categoryId = $request->get('category_id');
        $this->category->softDeleteData(['id' => $categoryId]);
        return ResponseHelper::delete();
    }

    public function all(Request $request)
    {
        $data = $this->category->allData([], 'desc', 'id', false);
        return ResponseHelper::select($data);
    }

    public function find(Request $request,$categoryId)
    {
        return ResponseHelper::select($this->category->findData(
            ['id' => $categoryId],
            ['*']
        ));
    }
}
