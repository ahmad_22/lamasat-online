<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\Customer\CreateCustomerRequest;
use App\Http\Requests\Customer\Customer\DeleteCustomerRequest;
use App\Http\Requests\Customer\Customer\UpdateCustomerRequest;
use App\Models\Customer\Customer;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class CustomerController extends Controller
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function create(CreateCustomerRequest $request)
    {
        $data = $request->only($this->customer->getFillable());
        $created = $this->customer->createData($data);
        return ResponseHelper::create($created);
    }

    public function update(UpdateCustomerRequest $request,$customerId)
    {
        $data = $request->only($this->customer->getFillable());
        $this->customer->updateData(['id' => $customerId], $data);
        return ResponseHelper::update();
    }

    public function delete(DeleteCustomerRequest $request,$customerId)
    {
        //$customerId = $request->get('customer_id');
        $this->customer->softDeleteData(['id' => $customerId]);
        return ResponseHelper::delete();
    }

    public function all(Request $request)
    {
        $data = $this->customer->allData();
        return ResponseHelper::select($data);
    }

    public function find(Request $request,$customerId)
    {
        $data = $this->customer->findData(['id'=>$customerId]);
        return ResponseHelper::select($data);
    }

}
