<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\Employee\CreateEmployeeRequest;
use App\Http\Requests\Employee\Employee\FindEmployeeRequest;
use App\Http\Requests\Employee\Employee\UpdateEmployeeRequest;
use App\Models\Employee\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class EmployeeController extends Controller
{
    protected $user, $employee;

    public function __construct(User $user, Employee $employee)
    {
        $this->user = $user;
        $this->employee = $employee;
    }

    public function create(CreateEmployeeRequest $request)
    {
        $dataEmp = $request->only($this->employee->getFillable());
        $created = $this->employee->createData($dataEmp);

        $dataUser = $request->only($this->user->getFillable());
        $dataUser['employee_id'] = $created->id;
        $created['user'] = $this->user->createData($dataUser);

        return ResponseHelper::create($created);
    }

    public function update(UpdateEmployeeRequest $request,$employeeId)
    {
        $dataEmp = $request->only($this->employee->getFillable());
        $this->employee->updateData(['id' => $employeeId], $dataEmp);

        $dataUser = $request->only($this->user->getFillable());
        $this->user->updateData(['id' => $employeeId], $dataUser);

        return ResponseHelper::update();
    }

    public function delete(FindEmployeeRequest $request,$employeeId)
    {
        //$employeeId = $request->get('employee_id');
        $data = $this->user->findData(['employee_id' => $employeeId]);
        if (!empty($data))
            $this->user->softDeleteData(['employee_id' => $employeeId]);

        $this->employee->softDeleteData(['id' => $employeeId]);
        return ResponseHelper::delete();
    }

    public function all(Request $request)
    {
        return ResponseHelper::select($this->employee->getData([], ['branch', 'department']));
    }

    public function find(FindEmployeeRequest $request,$employeeId)
    {
        return ResponseHelper::select($this->employee->findData(
            ['id' => $employeeId],
            ['*'],
            ['users', 'branch', 'department']
        ));
    }
}
