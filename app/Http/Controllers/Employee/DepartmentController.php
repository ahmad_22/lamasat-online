<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\Department\CreateDepartmentRequest;
use App\Http\Requests\Employee\Department\DeleteDepartmentRequest;
use App\Http\Requests\Employee\Department\UpdateDepartmentRequest;
use App\Models\Employee\Department;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class DepartmentController extends Controller
{
    private $department;

    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    public function create(CreateDepartmentRequest $request)
    {
        $data = $request->validated();
        $created = $this->department->createData($data);
        return ResponseHelper::create($created);
    }

    public function update(UpdateDepartmentRequest $request,$departmentId)
    {
        $data = $request->validated();
        $this->department->updateData(['id' => $departmentId], $data);
        return ResponseHelper::update();
    }

    public function delete(DeleteDepartmentRequest $request,$departmentId)
    {
        $data = $request->validated();
        $this->department->softDeleteData(['id' => $departmentId]);
        return ResponseHelper::delete();
    }

    public function all(Request $request)
    {
        $data = $this->department->allData([], 'desc', 'id', false);
        return ResponseHelper::select($data);
    }

    public function find(Request $request,$departmentId){
        return ResponseHelper::select($this->department->findData(
            ['id' => $departmentId],
            ['*']
        ));
    }
}
