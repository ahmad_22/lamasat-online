<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\Employee\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class UserController extends Controller
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login(LoginRequest $request)
    {
        $user = $request->only('username','password');
        $userLogin = $this->user->login($user);
        if (empty($userLogin))
            return ResponseHelper::authorizationFail();

        return ResponseHelper::operationSuccess($userLogin);
    }
}
