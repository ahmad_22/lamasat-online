<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\Branch\CreateBranchRequest;
use App\Http\Requests\Employee\Branch\DeleteBranchRequest;
use App\Http\Requests\Employee\Branch\UpdateBranchRequest;
use App\Models\Employee\Branch;
use Illuminate\Http\Request;
use Kouja\ProjectAssistant\Helpers\ResponseHelper;

class BranchController extends Controller
{
    private $branch;

    public function __construct(Branch $branch)
    {
        $this->branch = $branch;
    }

    public function create(CreateBranchRequest $request)
    {
        $data = $request->validated();
        $created = $this->branch->createData($data);
        return ResponseHelper::create($created);
    }

    public function update(UpdateBranchRequest $request,$branchId)
    {
        $data = $request->validated();
        $this->branch->updateData(['id' => $branchId], $data);
        return ResponseHelper::update();
    }

    public function delete(DeleteBranchRequest $request,$branchId)
    {
        $data = $request->validated();
        $this->branch->softDeleteData(['id' => $branchId]);
        return ResponseHelper::delete();
    }

    public function all(Request $request)
    {
        $data = $this->branch->allData([], 'desc', 'id', false);
        return ResponseHelper::select($data);
    }

    public function find(Request $request,$employeeId){
        return ResponseHelper::select($this->branch->findData(
            ['id' => $employeeId],
            ['*']
        ));
    }
}
