<?php


namespace App\Http\Requests\Employee\Department;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateDepartmentRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required', 'string'],
            'note'=>['nullable', 'string'],
        ];
    }
}
