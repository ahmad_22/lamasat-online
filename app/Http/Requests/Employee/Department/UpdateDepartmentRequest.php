<?php


namespace App\Http\Requests\Employee\Department;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class UpdateDepartmentRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        //    'department_id' => ['required', 'integer', Rule::exists('departments', 'id')
          //      ->whereNull('deleted_at')],
            'name' => ['required', 'string'],
            'note' => ['nullable', 'string'],
        ];
    }
}
