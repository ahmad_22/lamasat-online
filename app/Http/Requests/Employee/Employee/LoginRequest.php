<?php


namespace App\Http\Requests\Employee\Employee;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class LoginRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>['required', 'string'],
            'password'=>['required', 'string'],
        ];
    }
}
