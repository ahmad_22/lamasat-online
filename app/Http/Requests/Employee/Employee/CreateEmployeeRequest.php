<?php


namespace App\Http\Requests\Employee\Employee;

use App\Enums\GenderEnum;
use App\Enums\TypeSalaryEnum;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateEmployeeRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //Employee
            'full_name' => ['required', 'string'],
            'gender' => ['required', 'string', new EnumValue(GenderEnum::class)],
            'birthday' => ['nullable', 'date_format:Y-m-d'],
            'phone' => ['required', 'string'],
            'phone2' => ['nullable', 'string'],
            'address' => ['nullable', 'string'],
            'default_salary' => ['required', 'numeric'],
            'type_salary' => ['required', 'string', new EnumValue(TypeSalaryEnum::class)],
            'type_salary_of' => ['nullable', 'string'],
            'branch_id' => ['required', 'integer', Rule::exists('branches', 'id')
                ->whereNull('deleted_at')],
            'department_id' => ['required', 'integer', Rule::exists('departments', 'id')
                ->whereNull('deleted_at')],
            //user
            'username' => ['required', 'string', Rule::unique('users', 'username')],
            'password' => ['required', 'string', 'min:4', 'max:16'],
            'user_scope' => ['required', 'string'],
            'email' => ['nullable', 'string', Rule::unique('users', 'email')],
        ];
    }
}
