<?php


namespace App\Http\Requests\Employee\Branch;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class UpdateBranchRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           // 'branch_id' => ['required', 'integer', Rule::exists('branches', 'id')
             //   ->whereNull('deleted_at')],
            'name' => ['required', 'string'],
            'location' => ['required', 'string'],
            'note' => ['nullable', 'string'],
        ];
    }
}
