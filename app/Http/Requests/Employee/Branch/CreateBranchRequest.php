<?php


namespace App\Http\Requests\Employee\Branch;


use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateBranchRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required', 'string'],
            'location'=>['required', 'string'],
            'note'=>['nullable', 'string'],
        ];
    }
}
