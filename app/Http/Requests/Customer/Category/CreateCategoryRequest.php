<?php


namespace App\Http\Requests\Customer\Category;

use App\Enums\CustomerTypeEnum;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateCategoryRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'customer_type'=>['nullable', 'string', new EnumValue(CustomerTypeEnum::class)],
            'is_all' => ['nullable', 'boolean'],
            'customer_ids' => ['nullable','array'],
            'customer_ids.*' => ['nullable','integer',Rule::exists('customers','id')
                ->whereNull('deleted_at')],
            'discount' =>['nullable', 'numeric']
        ];
    }
}
