<?php


namespace App\Http\Requests\Customer\Category;

use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class UpdateCategoryRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'category_id' => ['required', 'integer', Rule::exists('categories', 'id')
              //  ->whereNull('deleted_at')],
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
        ];
    }
}
