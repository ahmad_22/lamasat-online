<?php


namespace App\Http\Requests\Customer\Customer;

use App\Enums\CustomerTypeEnum;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Validation\Rule;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class UpdateCustomerRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'customer_id' => ['required', 'integer', Rule::exists('customers', 'id')
              //  ->whereNull('deleted_at')],
            'name' => ['required', 'string'],
            'serial' => ['nullable', 'string'],
            'email' => ['nullable', 'string'],
            'phone' => ['required', 'string'],
            'phone2' => ['nullable', 'string'],
            'type' => ['required', 'string', new EnumValue(CustomerTypeEnum::class)],
            'address' => ['nullable', 'string'],
            'is_charge' => ['required', 'boolean'],
            'note' => ['nullable', 'string']
        ];
    }
}
