<?php


namespace App\Http\Requests\Customer\Customer;

use App\Enums\CustomerTypeEnum;
use BenSampo\Enum\Rules\EnumValue;
use Kouja\ProjectAssistant\Bases\BaseFormRequest;

class CreateCustomerRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'serial' => ['nullable', 'string'],
            'email' => ['nullable', 'string'],
            'phone' => ['required', 'string'],
            'phone2' => ['nullable', 'string'],
            'type' => ['required', 'string', new EnumValue(CustomerTypeEnum::class)],
            'address' => ['nullable', 'string'],
            'is_charge' => ['required', 'boolean'],
            'note' => ['nullable', 'string']
        ];
    }
}
