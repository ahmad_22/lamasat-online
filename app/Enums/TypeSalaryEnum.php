<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method percentege
 * @method fixed
 */
final class TypeSalaryEnum extends Enum
{
    const percentege =   'percentege';
    const fixed =   'fixed';
}
