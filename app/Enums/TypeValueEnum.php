<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class TypeValueEnum extends Enum
{
    const percentege =   'percentege';
    const fixed =   'fixed';
}
