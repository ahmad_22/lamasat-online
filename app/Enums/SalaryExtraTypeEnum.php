<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SalaryExtraTypeEnum extends Enum
{
    const discount =   'discount';
    const awred =   'awred';
}
