<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method female
 * @method male
 */
final class GenderEnum extends Enum
{
    const male = 'male';
    const female = 'female';
}
