<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method partner
 * @method big
 * @method small
 * @method direct
 */
final class CustomerTypeEnum extends Enum
{
    const partner =   'partner';
    const big =   'big';
    const small = 'small';
    const direct = 'direct';
}
