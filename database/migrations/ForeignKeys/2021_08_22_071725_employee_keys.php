<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmployeeKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->foreignId('branch_id')->constrained('branches','id');
            $table->foreignId('department_id')->constrained('departments','id');
        });
        
        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('employee_id')->constrained('employees','id');
        });

        Schema::table('time_records', function (Blueprint $table) {
            $table->foreignId('user_id')->constrained('users','id');
        });

        Schema::table('homework_day_employees', function (Blueprint $table) {
            $table->foreignId('employee_id')->constrained('employees','id');
            $table->foreignId('homework_day_id')->constrained('homework_days','id');
        });

        Schema::table('homework_day_switshes', function (Blueprint $table) {
            $table->foreignId('send_id')->constrained('employees','id');
            $table->foreignId('receive_id')->constrained('employees','id');
            $table->foreignId('homework_day_id')->constrained('homework_days','id');
        });

        Schema::table('homework_day_todos', function (Blueprint $table) {
            $table->foreignId('employee_id')->constrained('employees','id');
            $table->foreignId('homework_day_id')->constrained('homework_days','id');
        });

        Schema::table('salaries', function (Blueprint $table) {
            $table->foreignId('employee_id')->constrained('employees','id');
        });

        Schema::table('salary_extras', function (Blueprint $table) {
            $table->foreignId('employee_id')->constrained('employees','id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
