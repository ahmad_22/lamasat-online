<?php

use App\Enums\GenderEnum;
use App\Enums\TypeSalaryEnum;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->enum('gender',GenderEnum::getValues());
            $table->date('birthday')->nullable();
            $table->string('phone');
            $table->string('phone2')->nullable();
            $table->string('address')->nullable();
            $table->double('default_salary');
            $table->enum('type_salary',TypeSalaryEnum::getValues());
            $table->string('type_salary_of')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
