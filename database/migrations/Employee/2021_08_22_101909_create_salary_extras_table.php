<?php

use App\Enums\SalaryExtraTypeEnum;
use App\Enums\TypeValueEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_extras', function (Blueprint $table) {
            $table->id();
            $table->enum('extra_type', SalaryExtraTypeEnum::getValues());
            $table->text('note')->nullable();
            $table->enum('type',TypeValueEnum::getValues());
            $table->double('amount');
            $table->double('fullamount')->nullable();
            $table->double('percentage')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_extras');
    }
}
