<?php

namespace Database\Seeders\Employee;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
            [
                'name' => 'a1',
                'location' => 'syria',
                'note' => 'this branche end in 30/12/2025'
            ],
            [
                'name' => 'a2',
                'location' => 'home ',
                'note' => 'this branche my home'
            ]
        ]);
    }
}
