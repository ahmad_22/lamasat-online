<?php

namespace Database\Seeders\Employee;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            [
                'name' => 'design',
                'note' => 'qazesxrydcuyiuobpinjom[kp,lluk,gyjfht'
            ],
            [
                'name' => 'secretarial',
                'note' => ''
            ]
        ]);
    }
}
