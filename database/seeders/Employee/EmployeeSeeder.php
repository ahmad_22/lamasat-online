<?php

namespace Database\Seeders\Employee;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            [
                'id' => 1,
                'full_name' => 'empadmin',
                'gender' => 'male',
                'phone' => '1234567890',
                'default_salary' => '5000000',
                'type_salary' => 'fixed',
                'branch_id' => 1,
                'department_id' => 1
            ],
            [
                'id' => 2,
                'full_name' => 'empuser',
                'gender' => 'male',
                'phone' => '1234567890',
                'default_salary' => '3000000',
                'type_salary' => 'fixed',
                'branch_id' => 2,
                'department_id' => 2
            ]
        ]);
    }
}
