<?php

namespace Database\Seeders\Employee;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'username' => 'admin',
                'password' => Hash::make('1234'),
                'user_scope' => 'admin',
                'email'=> 'admin@admin.com',
                'employee_id'=> 1
            ],
            [
                'id' => 2,
                'username' => 'user',
                'password' => Hash::make('1234'),
                'user_scope' => 'user',
                'email'=> 'user@user.com',
                'employee_id'=> 2
            ],
        ]);
    }
}
