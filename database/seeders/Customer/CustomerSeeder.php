<?php

namespace Database\Seeders\Customer;

use App\Enums\CustomerTypeEnum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name'=>'ahmad',
                'serial'=>'a1',
                'email'=>'ahmad@a1.com',
                'phone'=>'12354678',
                'phone2'=>'12354678',
                'type'=>CustomerTypeEnum::direct,
                'address'=>'Aleppo',
                'is_charge'=>false,
                'note'=>'qweztrxycibpoml'
            ],
            [
                'name'=>'ali',
                'serial'=>'a22',
                'email'=>'ali@ali.com',
                'phone'=>'12354678',
                'phone2'=>'12354678',
                'type'=>CustomerTypeEnum::partner,
                'address'=>'Damascus',
                'is_charge'=>true,
                'note'=>'qweztrxycibpoml'
            ]
        ]);
    }
}
