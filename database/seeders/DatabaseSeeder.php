<?php

namespace Database\Seeders;

use Database\Seeders\Customer\CustomerSeeder;
use Database\Seeders\Employee\BranchSeeder;
use Database\Seeders\Employee\DepartmentSeeder;
use Database\Seeders\Employee\EmployeeSeeder;
use Database\Seeders\Employee\UserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BranchSeeder::class,
            DepartmentSeeder::class,
            EmployeeSeeder::class,
            UserSeeder::class,
            CustomerSeeder::class
        ]);
    }
}
